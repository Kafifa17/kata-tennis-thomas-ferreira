package katatennis.tennis;

public class Tennis {

	private Joueur joueur1;
	private Joueur joueur2;
	private int nbpointsgagnant;
	private int nbjeugagnant;
	private int nbtiebreakgagnant;
	public String avantage = " Avantage !";
	public String jeugagné = " a gagne le jeu !";
	public String setgagné = " a gagne le set !";
	public String egalite = "Egalite ";


	public Tennis(Joueur joueur1, Joueur joueur2,int nbpointsgagnant,int nbjeugagnant, int nbtiebreakgagnant ){
		this.setJoueur1(joueur1);
		this.setJoueur2(joueur2);
		this.setNbpointsgagnant(nbpointsgagnant);
		this.setNbjeugagnant(nbjeugagnant);
		this.setNbtiebreakgagnant(nbtiebreakgagnant);
	}

	public String Partie(){
		if(joueur1.getSet() < this.nbjeugagnant && joueur2.getSet() < this.nbjeugagnant) {
			return deroulementJeu();
		} else if (joueur1.getSet() == this.nbjeugagnant &&  joueur2.getSet() == this.nbjeugagnant) {
			return deroulementTieBreak();
		} else if (joueur1.getSet() >= this.nbjeugagnant ||  joueur2.getSet() >= this.nbjeugagnant) {
			if(Math.abs(joueur1.getSet() - joueur2.getSet())>=2){
				String gagnantset = joueurGagnantSet().getNom();
				reinitialiserSet();
				return gagnantset + this.setgagné;
			} else {
				return deroulementJeu();
			}
		} else {
			return scoreJeu() + scoreSet();
		}
	}

	public String deroulementJeu() {
		if(joueur1.getPoints() >= nbpointsgagnant && joueur2.getPoints() >= nbpointsgagnant) {
			if(Math.abs(joueur1.getPoints() - joueur2.getPoints())>=2){

				joueurGagnantJeu().gagnerJeu();
				String gagnantjeu = joueurGagnantJeu().getNom();
				reinitialiserPoints();
				return gagnantjeu + this.jeugagné + scoreSet();

			} else if(joueur1.getPoints() == joueur2.getPoints()) {
				return egalite+ scoreSet();
			} else {
				return joueurGagnantJeu().getNom() + this.avantage + scoreSet();
			}
		} else if(joueur1.getPoints() > nbpointsgagnant || joueur2.getPoints() > nbpointsgagnant){

			joueurGagnantJeu().gagnerJeu();
			String gagnantjeu = joueurGagnantJeu().getNom();
			reinitialiserPoints();
			return gagnantjeu + this.jeugagné +  scoreJeu() + scoreSet();
		} else {
			return  scoreJeu() + scoreSet();
		}
	}

	public String deroulementTieBreak() {
		if(joueur1.getPoints() >= this.nbtiebreakgagnant || joueur2.getPoints() >= this.nbtiebreakgagnant) {
			if(Math.abs(joueur1.getPoints() - joueur2.getPoints())>=2){

				joueurGagnantJeu().gagnerTieBreak();
				String gagnantset = joueurGagnantSet().getNom();
				reinitialiserSet();
				reinitialiserPoints();
				return gagnantset + this.setgagné;

			} else {
				return scoreTieBreak() + scoreSet();
			}
		} else {
			return scoreTieBreak() + scoreSet();
		}
	}

	public Joueur getJoueur1() {
		return joueur1;
	}

	public void setJoueur1(Joueur joueur1) {
		this.joueur1 = joueur1;
	}

	public Joueur getJoueur2() {
		return joueur2;
	}

	public void setJoueur2(Joueur joueur2) {
		this.joueur2 = joueur2;
	}

	public Joueur joueurGagnantJeu(){
		if (joueur1.getPoints()> joueur2.getPoints()) {
			return joueur1;
		} else {
			return joueur2;
		}
	}

	public void reinitialiserPoints() {
		this.joueur1.setPoints(0);
		this.joueur2.setPoints(0);
	}

	public void reinitialiserSet() {
		this.joueur1.setSet(0);
		this.joueur2.setSet(0);
	}


	public Joueur joueurGagnantSet(){
		if (joueur1.getSet()> joueur2.getSet())
			return joueur1;
		else
			return joueur2;
	}

	public String scoreSet() {
		return " Set :" + joueur1.getSet() + " - " + joueur2.getSet();
	}

	public String scoreJeu() {
		return " Jeu :" + joueur1.getJeu() + " - " + joueur2.getJeu();
	}

	public String scoreTieBreak() {
		return " TieBreak :" + joueur1.getPoints() + " - " + joueur2.getPoints();
	}

	public int getNbjeugagnant() {
		return nbpointsgagnant;
	}

	public void setNbpointsgagnant(int nbpointsgagnant) {
		this.nbpointsgagnant = nbpointsgagnant;
	}

	public int getNbsetgagnant() {
		return nbjeugagnant;
	}

	public void setNbjeugagnant(int nbjeugagnant) {
		this.nbjeugagnant = nbjeugagnant;
	}

	public int getNbtiebreakgagnant() {
		return nbtiebreakgagnant;
	}

	public void setNbtiebreakgagnant(int nbtiebreakgagnant) {
		this.nbtiebreakgagnant = nbtiebreakgagnant;
	}

}
