package katatennis.tennis.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import katatennis.tennis.Joueur;
import katatennis.tennis.Tennis;

public class TestTennis {

	public static Joueur joueur1;
	public static Joueur joueur2;
	public static Tennis partietennis;


	@Before
	public void setUpBeforeClass() throws Exception {
		joueur1 = new Joueur("Marc");
		joueur2 = new Joueur("Jean");
		partietennis = new Tennis(joueur1,joueur2,3,6,7);

	}

	@Test
	public void debutPartie() {

		assertEquals(partietennis.Partie(), " Jeu :0 - 0 Set :0 - 0");
	}

	@Test
	public void joueur1GagneJeu() {
		for(int i=0; i<4; i++) {
			joueur1.marquerPoint();
		}

		assertEquals(partietennis.Partie(), "Marc a gagne le jeu ! Jeu :0 - 0 Set :1 - 0");
	}

	@Test
	public void joueur1Joueur2ScorejeuSerre() {

		for(int i=0; i<3; i++) {
			joueur1.marquerPoint();
		}

		for(int i=0; i<2; i++) {
			joueur2.marquerPoint();
		}
		assertEquals(partietennis.Partie(), " Jeu :40 - 30 Set :0 - 0");
	}

	@Test
	public void joueur1joueur2egaliteJeu() {

		for(int i=0; i<4; i++) {
			joueur1.marquerPoint();
		}

		for(int i=0; i<4; i++) {
			joueur2.marquerPoint();
		}
		assertEquals(partietennis.Partie(), "Egalite  Set :0 - 0");
	}

	@Test
	public void joueur1Avantage() {

		for(int i=0; i<5; i++) {
			joueur1.marquerPoint();
		}

		for(int i=0; i<4; i++) {
			joueur2.marquerPoint();
		}
		assertEquals(partietennis.Partie(), "Marc Avantage ! Set :0 - 0");
	}

	@Test
	public void joueur1GagneSet() {

		for(int j = 0; j < 6;j++) {

			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
			}
			partietennis.Partie();
		}
		assertEquals(partietennis.Partie(), "Marc a gagne le set !");
	}

	@Test
	public void joueur1GagneSetSixQuatre() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 4;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}

		}

		for(int i = 0; i < 3;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		joueur1.marquerPoint();
		assertEquals(partietennis.Partie(),"Marc a gagne le jeu ! Jeu :0 - 0 Set :6 - 4");
		assertEquals(partietennis.Partie(), "Marc a gagne le set !");
	}

	@Test
	public void joueur1joueur2EgaliteSetCinqCinq() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		assertEquals(partietennis.Partie(), " Jeu :0 - 0 Set :5 - 5");
	}

	@Test
	public void joueur1joueur2EgaliteSetSixSix() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int i = 0; i < 4;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 4;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		assertEquals(partietennis.Partie(), " TieBreak :0 - 0 Set :6 - 6");
	}

	@Test
	public void joueur1GagneTieBreakSeptzero() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int i = 0; i < 4;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 4;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 6;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		assertEquals(partietennis.Partie(), " TieBreak :6 - 0 Set :6 - 6");
		joueur1.marquerPoint();
		assertEquals(partietennis.Partie(), "Marc a gagne le set !");
	}

	@Test
	public void joueur1gagneTieBreakHuitSix() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int i = 0; i < 4;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 4;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 6;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 6;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		assertEquals(partietennis.Partie(), " TieBreak :6 - 6 Set :6 - 6");
		joueur1.marquerPoint();
		assertEquals(partietennis.Partie(), " TieBreak :7 - 6 Set :6 - 6");
		joueur1.marquerPoint();
		assertEquals(partietennis.Partie(), "Marc a gagne le set !");
	}

	@Test
	public void joueur1EgaliteTieBreakHuitHuit() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int i = 0; i < 4;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 4;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 6;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}

		for(int i = 0; i < 6;i++) {
			joueur2.marquerPoint();
			partietennis.Partie();
		}

		assertEquals(partietennis.Partie(), " TieBreak :6 - 6 Set :6 - 6");
		joueur1.marquerPoint();
		joueur2.marquerPoint();
		assertEquals(partietennis.Partie(), " TieBreak :7 - 7 Set :6 - 6");
		joueur1.marquerPoint();
		joueur2.marquerPoint();
		assertEquals(partietennis.Partie(), " TieBreak :8 - 8 Set :6 - 6");
	}

	@Test
	public void joueur1gagneSetSeptcinq() {

		for(int j = 0; j < 5;j++) {
			for(int i = 0; i < 4;i++) {
				joueur1.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int j = 0; j < 5;j++) {

			for(int i = 0; i < 4;i++) {
				joueur2.marquerPoint();
				partietennis.Partie();
			}
		}

		for(int i = 0; i < 7;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}
		joueur1.marquerPoint();
		assertEquals(partietennis.Partie(),"Marc a gagne le jeu ! Jeu :0 - 0 Set :7 - 5");
		assertEquals(partietennis.Partie(), "Marc a gagne le set !");
	}

	@Test
	public void verificationJoueur1MeneJeu() {
		joueur1.marquerPoint();
		assertEquals(partietennis.joueurGagnantJeu(),joueur1);
	}

	@Test
	public void verificationJoueur1MeneSet() {
		for(int i = 0; i < 4;i++) {
			joueur1.marquerPoint();
			partietennis.Partie();
		}
		assertEquals(partietennis.joueurGagnantSet(),joueur1);
	}
}

