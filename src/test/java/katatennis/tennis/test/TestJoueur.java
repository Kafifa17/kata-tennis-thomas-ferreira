package katatennis.tennis.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import katatennis.tennis.Joueur;

public class TestJoueur {

	public static Joueur joueur1;


	@Before
	public void setUpBeforeClass() throws Exception {
		joueur1 = new Joueur("Marc");
	}

	@Test
	public void joueur1Nom() {

		assertEquals(joueur1.getNom(), "Marc");
	}
	
	@Test
	public void joueur1MarquerPoint() {
		joueur1.marquerPoint();
		assertEquals(joueur1.getPoints(), 1);
	}
	
	@Test
	public void joueur1MarquerPointJeu() {
		joueur1.marquerPoint();
		assertEquals(joueur1.getJeu(), 15);
	}
	
	@Test
	public void joueur1GagnerJeu() {
		joueur1.gagnerJeu();
		assertEquals(joueur1.getSet(), 1);
	}
	
	@Test
	public void joueur1GagnerSetauTieBreak() {
		joueur1.gagnerTieBreak();
		assertEquals(joueur1.getSet(), 0);
		joueur1.setSet(6);
		joueur1.gagnerTieBreak();
		assertEquals(joueur1.getSet(), 7);
	}

}
